from helpers.base_test_class import BaseTestClass
from helpers.wrappers import screenshot_decorator


class LostHatProductPageTests(BaseTestClass):


    def assert_element_text(self, driver, xpath, expected_text):
        """Comparing expected text with observed value from web element

        :param driver: webdriver instance
        :param xpath: xpath to element with text to be observed
        :param expected_text: text what we expecting to be found
        """
        element = driver.find_element_by_xpath(xpath)
        element_text = element.text
        self.assertEqual(expected_text, element_text, f'Expected text differ from actual on page: {driver.current_url}')

    @screenshot_decorator
    def test_check_product_name(self):
        expected_text = 'HUMMINGBIRD PRINTED T-SHIRT'
        driver = self.ef_driver
        driver.get(self.sample_product_url)
        xpath = '//*[@class="col-md-6"]//*[@itemprop="name"]'
        self.assert_element_text(driver, xpath, expected_text)

    @screenshot_decorator
    def test_check_product_price(self):
        expected_text = 'PLN23.52'
        driver = self.ef_driver
        driver.get(self.sample_product_url)
        xpath = '//*[@class="current-price"]//*[@itemprop="price"]'
        self.assert_element_text(driver, xpath, expected_text)
