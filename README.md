# Kurs testów automatycznych w Selenium i Python
Projekt z testami automatycznymi z kursu jaktestowac.pl

## Zadania wykonane w ramach kursu
- testy front page sklepu internetowego
- test logowania do konta
- testy wyszukiwarki produktów
- test koszyka zakupów
- testy typu Smoke
- testy typu Sanity
- test Suite
- tworzenie raportów za pomocą narzędzia Allure
 

## Źródło
[![alt text](https://con.jaktestowac.pl/wp-content/uploads/brand/jaktestowac_small.png)](https://jaktestowac.pl/git-dla-testerow)

## Technologie
- Git 2.26
- Pycharm 2019.2.5
- Python 3.7
- Selenium Webdriver
- allure 2.13.1
- Windows 10
