import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.events import EventFiringWebDriver
from helpers.screenshot_listener import ScreenshotListener
from helpers.wrappers import screenshot_decorator

class LostHatSmokeTests(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.base_url = 'https://autodemo.testoneo.com/en/'
        self.art_url = self.base_url + '9-art'
        self.clothes_url = self.base_url + '3-clothes'
        self.accessories_url = self.base_url + '6-accessories'
        self.login_url = self.base_url + 'login'
        driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
        self.ef_driver = EventFiringWebDriver(driver, ScreenshotListener())

    @classmethod
    def tearDownClass(self):
        self.ef_driver.quit()

    def get_page_title(self, url):
        self.ef_driver.get(url)
        return self.ef_driver.title

    def assert_title(self, url, expected_title):
        actual_title = self.get_page_title(url)
        self.assertEqual(expected_title, actual_title,
                         f'Expected {expected_title} differ from actual title {actual_title} on page: {url}')

    @screenshot_decorator
    def test_base_page_title(self):
        expected_title = 'Lost Hat'
        self.assert_title(self.base_url, expected_title)
    @screenshot_decorator
    def test_product_art_page_title(self):
        expected_title = 'Art'
        self.assert_title(self.art_url, expected_title)
    @screenshot_decorator
    def test_product_clothes_page_title(self):
        expected_title = 'Clothes'
        self.assert_title(self.clothes_url, expected_title)
    @screenshot_decorator
    def test_product_accessories_page_title(self):
        expected_title = 'Accessories'
        self.assert_title(self.accessories_url, expected_title)
    @screenshot_decorator
    def test_login_page_title(self):
        expected_title = 'Login'
        self.assert_title(self.login_url, expected_title)
    @screenshot_decorator
    def test_smoke_search_on_main_page(self):
        min_expected_elements = 4
        search_phase = 'mug'
        search_xpath = '//*[@name="s"]'
        result_xpath = '//*[@class="product-miniature js-product-miniature"]'

        self.ef_driver.get(self.base_url)
        search_input_element = self.ef_driver.find_element_by_xpath(search_xpath)
        search_input_element.clear()
        search_input_element.send_keys(search_phase)
        search_input_element.send_keys(Keys.ENTER)

        result_elements = self.ef_driver.find_elements_by_xpath(result_xpath)
        with self.subTest('Search engine test'):
            self.assertLessEqual(min_expected_elements, len(result_elements),
                                 f'expected elements {min_expected_elements}, actual elements {len(result_elements)}')
