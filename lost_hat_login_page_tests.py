from helpers import functional_helpers as fh
from helpers.base_test_class import BaseTestClass
from helpers.wrappers import screenshot_decorator


class LostHatLoginPageTests(BaseTestClass):

    def assert_element_text(self, driver, xpath, expected_text):
        """Comparing expected text with observed value from web element

        :param driver: webdriver instance
        :param xpath: xpath to element with text to be observed
        :param expected_text: text what we expecting to be found
        """
        element = driver.find_element_by_xpath(xpath)
        element_text = element.text
        self.assertEqual(expected_text, element_text, f'Expected text differ from actual on page: {driver.current_url}')

    @screenshot_decorator
    def test_login_text_header(self):
        expected_text_header = 'Log in to your account'
        xpath = '//header[@class="page-header"]'
        driver = self.ef_driver

        driver.get(self.login_url)
        self.assert_element_text(driver, xpath, expected_text_header)
    @screenshot_decorator
    def test_correct_login(self):
        # expected_text is a user name and user surname used during registration
        expected_text = 'bixon one'
        user_email = 'kgarus92@gmail.com'
        user_pass = '1qaz2wsx'
        xpath = '//a[@class="account"]/*[@class="hidden-sm-down"]'
        driver = self.ef_driver
        driver.get(self.login_url)
        fh.user_login(driver, user_email, user_pass)
        self.assert_element_text(driver, xpath, expected_text)
    @screenshot_decorator
    def test_incorrect_login(self):
        # expected_text is a user name and user surname used during registration
        expected_text = 'Authentication failed.'
        user_email = 'asdfasdf2@mail.com'
        user_pass = '12345678'
        xpath = '//*[@id="content"]//*[@class="alert alert-danger"]'
        driver = self.ef_driver
        driver.get(self.login_url)
        fh.user_login(driver, user_email, user_pass)
        self.assert_element_text(driver, xpath, expected_text)